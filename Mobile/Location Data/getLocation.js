Titanium.Geolocation.preferredProvider = Titanium.Geolocation.PROVIDER_GPS;
Titanium.Geolocation.accuracy = Titanium.Geolocation.ACCURACY_BEST;
Titanium.Geolocation.distanceFilter = 1;
Ti.Geolocation.purpose = "Find deals near you!";

// GLOBAL LISTENER


function getMyLocation(){
// make the API call
Ti.Geolocation.getCurrentPosition(function(e) {
  
  // do this stuff when you have a position, OR an error
  if (e.error) {
    Ti.API.error('geo - current position' + e.error);
    return;
  }
  else
  {
  				longitude = e.coords.longitude;
				latitude = e.coords.latitude;
  
  
  			Titanium.Geolocation.reverseGeocoder(latitude,longitude,function(evt)
		        {
		            Ti.API.info("reverse geolocation result = "+JSON.stringify(evt));
		          var places = evt.places;
		 
		          myLat=places[0].latitude;
		          myLon=places[0].longitude;
		          // alert(currentLocation);
		        
		         getDeals(page);
		         Ti.API.info("reverse geolocation result = "+JSON.stringify(evt));
		           
		        });
		       }//end if

});

		
}
getMyLocation();