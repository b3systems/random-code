/**this checks if android or ios***/

var isAndroid=false;
var isIphone=false;

if(Titanium.Platform.osname=='android'){
	isAndroid=true;
	isIphone=false;
}
if(Titanium.Platform.osname=='iphone'){
	isAndroid=false;
	isIphone=true;
}
/*****/

/****this is like php trim****/
function trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}
function ltrim(stringToTrim) {
	return stringToTrim.replace(/^\s+/,"");
}
function rtrim(stringToTrim) {
	return stringToTrim.replace(/\s+$/,"");
}
/***end trim****/

/***this removes html from a string****/
function stripHTML(str){
str=str.replace(/<\s*br\/*>/gi, "");
str=str.replace(/<\s*a.*href="(.*?)".*>(.*?)<\/a>/gi, " $2 (Link->$1) ");
str=str.replace(/<\s*\/*.+?>/ig, "");
str=str.replace(/ {2,}/gi, " ");
str=str.replace(/\n+\s*/gi, "")
return str;
}
/***end html remove****/

/***this removes apostrophes from a string***/
function removeApostrophe(mystring)
{
var newstring=mystring.replace(/(&#8220;)|(&#8221;)|(&#8217;)[""\u8220\u8221\u8217]/g, "'");
	newstring=newstring.replace(/(&#8216;)|(&#8217;)|[''\u8216\u8217]/g, "'");
	newstring=newstring.replace("&#8217;","'");
	newstring=newstring.replace("&#8211;","-");
	newstring=newstring.replace("&#39;","'");
return newstring;
}
/*****/

/***this is used for animation in android****/

function convertToPixel(dpValue)
{
	var baseDpi = 160;
    var dpi = Ti.Platform.displayCaps.dpi;
    var result =(dpValue * (dpi / baseDpi));
    return result;
 
}
/***end animation***/

/*****this is used to check if a string is an integer***/
function isInt(value){ 
  if((parseFloat(value) == parseInt(value)) && !isNaN(value)){
      return true;
  } else { 
      return false;
  } 
}
/***end integer function***/

/***this gets location***/
function getLocation(){
	Titanium.Geolocation.preferredProvider = Titanium.Geolocation.PROVIDER_GPS;
	Titanium.Geolocation.accuracy = Titanium.Geolocation.ACCURACY_BEST;
	Titanium.Geolocation.distanceFilter = 1;
	Ti.Geolocation.purpose = "Message goes here";

	Ti.Geolocation.addEventListener('location', function(e) {

	  // do this stuff when you have a position, OR an error



	  				longitude = e.coords.longitude;
					latitude = e.coords.latitude;
					Titanium.App.Properties.setString("lat",latitude);
					Titanium.App.Properties.setString("lon",longitude);

			



	});
}
//force Android to be portrait
function androidPortrait(){
	if (Ti.Platform.osname == 'android'){
	            Ti.Gesture.addEventListener('orientationchange', function(e) {

	              Ti.Android.currentActivity.setRequestedOrientation(Ti.Android.SCREEN_ORIENTATION_PORTRAIT);
	            });
	        }
}
